# Generator tematów

Aplikacja umożliwia generowanie gotowego tematu w "bbcode" dla skryptu forum vBulletin. Generator pobiera dane z serwisu "gry-online.pl". Zasada działania jest bardzo prosta, wystarczy wkleić link z ww serwisu, nacisnąć przycisk "generuj" i skopiować gotowy kod "bbcode".


# Stack technologiczny

* C#
* Visual Studio

## Uruchamianie

Aby uruchomić projekt należy sklonować repozytorium, a następnie otworzyć je w Visual Studio. W ten sposób otrzymamy wykonywalny plik .exe programu.

## Licencja
MIT