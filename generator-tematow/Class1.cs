﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace generator_tematow
{
    class Thread
    {
        public String title { set; get; }
        public String cover { set; get; }
        public List<String> gallery { set; get; }
        public String description { set; get; }
        public String story { set; get; }
        public String mechanics { set; get; }
        public String gameModes { set; get; }
        public String technicalIssues { set; get; }
        public String gameModes2 { set; get; }
        public String requirements { set; get; }
        public String sizeUpload { set; get; }
        public String kindOfUpload { set; get; }
        public String contentUpload { set; get; }
        public String language { set; get; }
        public String dataRecovery { set; get; }
        public String password { set; get; }
        public List<String> hostNames { set; get; }
        public List<string> links { set; get; }

        public Thread()
        {
            gallery = new List<string>();
            hostNames = new List<string>();
            links = new List<string>();
        }

        public void saveToFile(string path)
        {
            if (File.Exists(@""+path))
            {
                File.Delete(@""+path);
            }

            using (System.IO.StreamWriter file =
            new System.IO.StreamWriter(@""+path, true))
            {
                file.WriteLine("[CENTER][SIZE=6][COLOR=rgb(0, 89, 179)][B]");
                file.WriteLine(title);
                file.WriteLine("[/B][/COLOR][/SIZE]");
                file.WriteLine("[IMG]");
                file.WriteLine(cover);
                file.WriteLine("[/IMG]");
                file.Write("[SIZE=5][U][COLOR=#0059b3]Opis[/COLOR][/U][/SIZE]");
                file.WriteLine(description);
                file.WriteLine("[SIZE=5][U][COLOR=#0059b3]Fabuła[/COLOR][/U][/SIZE]");
                file.WriteLine(story);
                file.WriteLine("");
                file.WriteLine("[SIZE=5][U][COLOR=#0059b3]Mechanika[/COLOR][/U][/SIZE]");
                file.WriteLine(mechanics);
                file.WriteLine("");
                file.WriteLine("[SIZE=5][U][COLOR=#0059b3]Tryby gry[/COLOR][/U][/SIZE]");
                file.WriteLine(gameModes);
                file.WriteLine("[SIZE=5][U][COLOR=#0059b3]Kwestie techniczne[/COLOR][/U][/SIZE]");
                file.WriteLine(technicalIssues);
                file.WriteLine(gameModes2);
                file.WriteLine("[SIZE=5][U][COLOR=#0059b3]Wymagania sprzętowe[/COLOR][/U][/SIZE]");
                file.WriteLine(requirements);
                file.WriteLine("");
                file.WriteLine("[CENTER][SIZE=4][COLOR=#808080][SPOILER='Screeny']");
                foreach (String pic in gallery)
                {
                    file.Write("[IMG]");
                    file.Write(pic);
                    file.WriteLine("[/IMG]");
                }
                file.WriteLine("[/SPOILER][/COLOR][/SIZE][/CENTER]");
                file.WriteLine("[SIZE=5][U][COLOR=#0059b3]Opis uploadu[/COLOR][/U][/SIZE]");
                file.WriteLine("[code]");
                if (sizeUpload.Length > 0)
                {
                    file.Write("Rozmiar uploadu: ");
                    file.WriteLine(sizeUpload);
                }
                if (kindOfUpload.Length > 0)
                {
                    file.Write("Rodzaj kompresji: ");
                    file.WriteLine(kindOfUpload);
                }
                if (contentUpload.Length > 0)
                {
                    file.Write("Zawartość: ");
                    file.WriteLine(contentUpload);
                }
                if (language.Length > 0)
                {
                    file.Write("Wersja językowa: ");
                    file.WriteLine(language);
                }
                if (dataRecovery.Length > 0)
                {
                    file.Write("Dane naprawcze: ");
                    file.WriteLine(dataRecovery);
                }
                if (password.Length > 0)
                {
                    file.Write("Hasło: ");
                    file.WriteLine(password);
                }
                file.WriteLine("[/code]");

                if (links.Count() > 0)
                {
                    file.WriteLine("[SIZE=5][U][COLOR=#0059b3]Linki[/COLOR][/U][/SIZE]");
                    file.WriteLine("");
                    for (int i = 0; i < hostNames.Count(); i++)
                    {
                        file.WriteLine(hostNames[i]);
                        file.WriteLine("[code]");
                        file.WriteLine(links[i]);
                        file.WriteLine("[/code]");
                    }
                }

                file.WriteLine("");
                file.WriteLine("");
                file.WriteLine("[b]Informacje pobrane ze strony www.gry-online.pl[/b]");
            }
        }
    }
}
