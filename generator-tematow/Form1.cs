﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HtmlAgilityPack;
using System.Web;
using System.IO;

namespace generator_tematow
{
    public partial class Form1 : Form
    {
        Thread thread;
        public Form1()
        {
            InitializeComponent();
        }


        private void button1_Click(object sender, EventArgs e)
        {
            thread = new Thread();
            WebClient webClient = new WebClient();
            String address = Convert.ToString(textBox1.Text);
            string pageContent = webClient.DownloadString(address);
            var htmlDocument = new HtmlAgilityPack.HtmlDocument();
            htmlDocument.LoadHtml(pageContent);


            HtmlNode galleryNode = htmlDocument.DocumentNode.SelectSingleNode("//div[@class='game-galeria']");

            int cnt = 0;
            String part = "http://www.gry-online.pl";
            foreach (var picture in galleryNode.Descendants("a"))
            {
                String fullURL = part + picture.Attributes["href"].Value;
                string galleryContent = webClient.DownloadString(fullURL);
                var pictureDocument = new HtmlAgilityPack.HtmlDocument();
                pictureDocument.LoadHtml(galleryContent);
                HtmlNode pictureNode = pictureDocument.DocumentNode.SelectSingleNode("//img[@id='screenshot']");
                String pictureURL = pictureNode.Attributes["src"].Value;
                thread.gallery.Add(pictureURL.Remove(0, 2));
                cnt++;
                if (cnt == 4)
                    break;
            }

            HtmlNode titleNode = htmlDocument.DocumentNode.SelectSingleNode("//header/h1");
            thread.title = titleNode.InnerText;

            HtmlNode okladkaNode = htmlDocument.DocumentNode.SelectSingleNode("//img[@class='game-box']");
            thread.cover = part + okladkaNode.Attributes["src"].Value;

            HtmlNode descriptionNode = htmlDocument.DocumentNode.SelectSingleNode("//div[@id='s016opisenc']");
            thread.description = descriptionNode.InnerText;

            var allH3 = htmlDocument.DocumentNode.SelectNodes("//h3");
            foreach (HtmlNode node in allH3)
            {
                if (node.InnerText.Equals("Fabuła"))
                {
                    HtmlNode fabNode = node.NextSibling;
                    thread.story = fabNode.InnerText;
                }
                else if (node.InnerText.Equals("Mechanika"))
                {
                    HtmlNode mNode = node.NextSibling;
                    thread.mechanics = mNode.InnerText;
                }
                else if (node.InnerText.Equals("Tryby gry"))
                {
                    HtmlNode mNode = node.NextSibling;
                    thread.gameModes = mNode.InnerText;
                }
                else if (node.InnerText.Equals("Kwestie techniczne"))
                {
                    HtmlNode mNode = node.NextSibling;
                    thread.technicalIssues = mNode.InnerText;
                }

            }

            var all = htmlDocument.DocumentNode.SelectNodes("//*[contains(@class,'word-txt')]");

            foreach (HtmlNode node in all)
            {
                var src = node.InnerText;
                var result = src.Contains("Wymagania");
                if (result)
                {
                    thread.requirements = node.InnerText.Remove(0,60);
                    continue;
                }

                result = src.Contains("Tryb gry:");
                if (result)
                {
                    thread.gameModes2 = node.InnerText.Remove(0,200);
                    continue;
                }
            }
            MessageBox.Show("Temat został wygenerowany! Możesz go teraz zapisać do pliku.");
            button2.Enabled = true;
            button1.Enabled = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            getDataForms();
            SaveFileDialog path = new SaveFileDialog();
            path.Filter = "Pliki tekstowe|*.txt"; ;
            if (path.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string filePath = Path.GetFullPath(path.FileName);
                thread.saveToFile(filePath);
                MessageBox.Show("Plik został zapisany.");
            }
            button2.Enabled = false;
            button1.Enabled = true;
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if(checkBox1.Checked == true)
            {
                textBox8.Enabled = true;
                textBox9.Enabled = true;
            }
            else
            {
                textBox8.Enabled = false;
                textBox9.Enabled = false;
            }
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox2.Checked == true)
            {
                textBox10.Enabled = true;
                textBox11.Enabled = true;
            }
            else
            {
                textBox10.Enabled = false;
                textBox11.Enabled = false;
            }
        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox3.Checked == true)
            {
                textBox12.Enabled = true;
                textBox13.Enabled = true;
            }
            else
            {
                textBox12.Enabled = false;
                textBox13.Enabled = false;
            }
        }

        private void getDataForms()
        {
            thread.sizeUpload = textBox2.Text;
            thread.kindOfUpload = textBox3.Text;
            thread.contentUpload = textBox4.Text;
            thread.language = textBox5.Text;
            thread.dataRecovery = textBox6.Text;
            thread.password = textBox7.Text;

            if(checkBox1.Checked == true)
            {
                thread.hostNames.Add(textBox8.Text);
                thread.links.Add(textBox9.Text);
            }

            if (checkBox2.Checked == true)
            {
                thread.hostNames.Add(textBox10.Text);
                thread.links.Add(textBox11.Text);
            }

            if (checkBox3.Checked == true)
            {
                thread.hostNames.Add(textBox12.Text);
                thread.links.Add(textBox13.Text);
            }
        }
    }
}
